<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Prismic\Api;

class PrismicServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Prismic\API', function ($app) {
           return Api::get(env('PRISMIC_URL'), env('PRISMIC_TOKEN'));
        });
    }
}

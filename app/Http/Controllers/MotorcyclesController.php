<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MotorcyclesController extends Controller
{

    private $api;

    /**
     * Motorcycles constructor.
     */
    public function __construct()
    {
        $this->api = resolve('Prismic\API');
    }

    public function get()
    {
        $response = $this->api->forms()->everything->ref($this->api->master())->submit();

        $document = $response->getResults()[0];

        return $document->getType();
    }
}
